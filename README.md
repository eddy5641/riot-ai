# Riot-AI
The stupidly good bot

Powered by some Vision APIs, AI, and OCR

# About
Riot-AI is my first attempt at making my own game-play bot, built using my own
platform (SandFire)

Riot-AI is written in a beautiful hybrid of C++ and C# because screw Rust
(@Lucas do you feel sad now)

Are you riot games and what this project dead?
I'll try my best to take it down, but you have to say this is a cool project

# Purpose
Me and my friend are trying to make 2 different bots for league of legends.
The goal is to make a custom game with only those two bots and see which bot 
is victorious. The games would be a 1v1 and a 5v5.

I think that this could also be (potentially) a super powerful practice tool
to be able to adjust difficultly on the fly.

# Using Riot-AI
Note: Riot-AI was designed to NOT BE USED FOR LEVEL BOTS, and can only be used 
in custom games. 

Level 30 accounts can also enter games with Riot-AI but like it prob isn't THAT 
good (yet. tm)

## Getting started
Download and install IcyWind. In the IcyWind Plugin Store, add the 
IcyWind.SandFire.RiotAI plugin.

RiotAI uses SandFire with the League of Legends GameClient. This means that 
IcyWind MUST launch the GameClient with SandFire "semi-internal" injection.

## Using Riot-AI
Riot AI should auto-detect enemy champions and allied champions with vision
api, and draw orbwalkers, which should be auto-tracked onto all champions.

This means that Riot-AI has been successful and is running. You should also see
SandFire.Riot-AI at the top of the screen. Riot-AI will block any user movement
commands.

# How it works
## Champion Detection + Range Drawing
Riot-AI uses Riot Games stupidity against them. During the stupidly long
champion select screen, Riot-AI uses OCR to read allied champions and enemy
champions. Riot-AI can than deduce what team, side they are on. This is than fed
into a stupid algorithm to guess where each of the enemy players is going
and pick starting items that are the most appropiate for the ememy players.

Range drawing is handled by than using Champion Stats and than just drawing
an orbwalker over them by using a Vision API

## Spell detection
Riot-AI uses both sound and visuals to detect skill shots. Sounds are used to
identify which skillshot is being used and tries to use Vision API to detect 
where the skillshot is going

When it sees an update on the screen that looks like a skillshot it registers
the drawing on the screen.

## Spell dodging
Riot-AI finally uses AI to guess if it is better to get hit by the skillshot or
to try and dodge it. It uses a smarter stutter step algorithm and tries not to
look stupid (like scripts)

## Objective guessing
Riot-AI uses AI to try and guess risk of getting certain objectives by 
factoring in enemy mini-map locations, known ward locations, probable ward 
locations, probable enemy locations, possible ward locations, possible
enemy possitions, estimated enemy appear time, KDA of all players, and number of
alive and dead allies/enemies. This means that if Riot-AI predicts that it is 
safe enough to run one of these objectives it may ping or attempt to get it by
itself.

## CS
Just looks at minions hp bar and guesses if it can last hit it and which order
it should last hit.

## Kill detection
Riot-AI uses sound and Vision AI to determine kills. Sometimes it will check 
rapidly using tab (you will see if flash really fast).

## Enemy gold detection
Riot-AI tries to guess how much gold each enemy has and factors that into 
gameplay decisions.

#DISCLAIMER
If you get banned, or the bot gets banned, I don't care.

This project is not sponsored, powered by, or in any way affiliated with Riot 
Games. It probably is frowned upon by Riot Games